<?php get_header(); ?>
<?php if(is_user_logged_in()) { ?>
		<section class="block fichas-container">
			<h1>Evaluaciones para distribuidores Elgon</h1>			
		</section>
		<ul class="listado-etapas block">
			<li class="etapa-active">
				<div class="eval-doc">
					<h4 class="desc-guia">
						<a target="_blank" href="<?php echo get_template_directory_uri(); ?>/guias/evaluacion-elgon-etapa-1.pdf">Descarga la guía para la etapa 1</a>
					</h4>
				</div>
				<div class="eval-cont">
					<h3>Etapa 1</h3>
					<p>Evaluación sobre: Conocimiento de marca y de producto.</p>
					<?php
						$resultadoeval1 = get_user_meta(get_current_user_id(),'_resultadoeval1',true);
						$conteo = count($resultadoeval1);
						$horario = get_user_meta(get_current_user_id(),'_horarioeval1',true);
						if($conteo != '' && $horario != '')
						{
							if($conteo > 8)
							{
								echo '<p class="correcta"><strong>Resultado de tu evaluación: '.$conteo.' / 13</strong></p>';
							}
							else
							{
								echo '<p class="incorrecta"><strong>Resultado de tu evaluación: '.$conteo.' / 13</strong></p>';
							}
							echo '
								<p>Fecha y horario de tu evaluación: <strong>'.$horario.'</strong><p>
							';
						}
						else {
							echo '
								<p>Aún no has tomado esta evaluación</p>
								<a class="responde-ahora" href="'.home_url().'/distribuidor-home/evaluaciones/evaluacion-elgon-etapa-1/">Responde ahora a las preguntas</a>
							';
						}					
					?>
				</div>
			</li>
			<?php
				$resultadoeval1 = get_user_meta(get_current_user_id(),'_resultadoeval1',true);
				$conteo = count($resultadoeval1);
				if($resultadoeval1 == '' || $conteo < 8)
				{
					echo '<li class="etapa-unactive">';
				}
				else
				{
					echo '<li class="etapa-active">';
				}
			?>
				<div class="eval-doc">
					<h4 class="desc-guia">
						<a target="_blank" href="<?php echo get_template_directory_uri(); ?>/guias/evaluacion-elgon-etapa-2.pdf">Descarga la guía para la etapa 2</a>
					</h4>
				</div>
				<div class="eval-cont">
					<h3>Etapa 2</h3>
					<p>Evaluación sobre: Prospectos.</p>
					<?php
						$resultadoeval2 = get_user_meta(get_current_user_id(),'_resultadoeval2',true);
						$conteo = count($resultadoeval2);
						$horario = get_user_meta(get_current_user_id(),'_horarioeval2',true);
						if($conteo != '' && $horario != '')
						{
							if($conteo > 5)
							{
								echo '<p class="correcta"><strong>Resultado de tu evaluación: '.$conteo.' / 6</strong></p>';
							}
							else
							{
								echo '<p class="incorrecta"><strong>Resultado de tu evaluación: '.$conteo.' / 6</strong></p>';
							}
							echo '
								<p>Fecha y horario de tu evaluación: <strong>'.$horario.'</strong><p>
							';
						}
						else {
							echo '
								<p>Aún no has tomado esta evaluación</p>
								<a class="responde-ahora" href="'.home_url().'/distribuidor-home/evaluaciones/evaluacion-elgon-etapa-2/">Responde ahora a las preguntas</a>
							';
						}					
					?>
				</div>
			</li>			
			<?php
				$resultadoeval2 = get_user_meta(get_current_user_id(),'_resultadoeval2',true);
				$conteo = count($resultadoeval2);
				if($resultadoeval1 == '' || $conteo < 5)
				{
					echo '<li class="etapa-unactive">';
				}
				else
				{
					echo '<li class="etapa-active">';
				}
			?>
			<div class="eval-doc">
				<h4 class="desc-guia">
					<a target="_blank" href="<?php echo get_template_directory_uri(); ?>/guias/evaluacion-elgon-etapa-3.pdf">Descarga la guía para la etapa 3</a>
				</h4>
			</div>
			<div class="eval-cont">
					<h3>Etapa 3</h3>
					<p>Evaluación sobre: Organización del tiempo.</p>
					<?php
						$resultadoeval3 = get_user_meta(get_current_user_id(),'_resultadoeval3',true);
						$conteo = count($resultadoeval3);
						$horario = get_user_meta(get_current_user_id(),'_horarioeval3',true);
						if($conteo != '' && $horario != '')
						{
							if($conteo > 5)
							{
								echo '<p class="correcta"><strong>Resultado de tu evaluación: '.$conteo.' / 9</strong></p>';
							}
							else
							{
								echo '<p class="incorrecta"><strong>Resultado de tu evaluación: '.$conteo.' / 9</strong></p>';
							}
							echo '
								<p>Fecha y horario de tu evaluación: <strong>'.$horario.'</strong><p>
							';
						}
						else {
							echo '
								<p>Aún no has tomado esta evaluación</p>
								<a class="responde-ahora" href="'.home_url().'/distribuidor-home/evaluaciones/evaluacion-elgon-etapa-3/">Responde ahora a las preguntas</a>
							';
						}					
					?>
			</div>
			</li>
			<?php
				$resultadoeval3 = get_user_meta(get_current_user_id(),'_resultadoeval3',true);
				if($resultadoeval3 == '')
				{
					echo '<li class="etapa-unactive">';
				}
				else
				{
					echo '<li class="etapa-active">';
				}
			?>			
				<h3>Etapa 4</h3>
				<p>Evaluación sobre: Por definir.</p>
			</li>
		</ul>
	<?php } else { ?>
		<?php echo do_shortcode('[custom-login-form]'); ?>
	<?php } ?>
<?php get_footer(); ?>