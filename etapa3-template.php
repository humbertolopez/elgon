<?php
	/*
	Template name: Etapa 3 evaluación
	*/
	get_header();
?>
<?php if(is_user_logged_in()) { ?>
		<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
		<section class="block fichas-container">
			<h1><?php the_title(); ?></h1>
			<?php the_content(); ?>
		</section>
		<form method="POST" action="" name="<?php echo $post->post_name; ?>">
			<ol class="listado-evaluacion block">
				<li class="pregunta-evaluacion">
					<h3>¿Qué es el M.O.T.E.?</h3>
					<p><input type="radio" name="eval3quest1" value="a"><label>Motivación, Oportunidad, Técnica y Estratégia</label></p>
					<p><input type="radio" name="eval3quest1" value="b"><label>Motivación, Organización, Técnica y Entrega</label></p>
					<p><input type="radio" name="eval3quest1" value="c"><label>Mayoreo, Oportunidad, Trabajo en Equipo</label></p>
				</li>
				<li class="pregunta-evaluacion">
					<h3>¿Qué es algo que te permite saber una buena organización?</h3>
					<p><input type="radio" name="eval3quest2" value="a"><label>Cuál es mi situación y corregir el rumbo</label></p>
					<p><input type="radio" name="eval3quest2" value="b"><label>Qué es lo que quiero en la vida</label></p>
					<p><input type="radio" name="eval3quest2" value="c"><label>Cómo mejorar mi aspecto personal</label></p>
				</li>
				<li class="pregunta-evaluacion">
					<h3>¿Cuál es la reacción natural que tenemos ante algo que no conocemos?</h3>
					<p><input type="radio" name="eval3quest3" value="a"><label>Pesimismo</label></p>
					<p><input type="radio" name="eval3quest3" value="b"><label>Rechazo</label></p>
					<p><input type="radio" name="eval3quest3" value="c"><label>Afecto</label></p>
				</li>
				<li class="pregunta-evaluacion">
					<h3>Para aprender a ser organizado debes de aprender 2 cosas importantes, ¿Cuales son?</h3>
					<p><input type="radio" name="eval3quest4" value="a"><label>Mejorar autoconocimiento y entender a mis clientes</label></p>
					<p><input type="radio" name="eval3quest4" value="b"><label>Respeto y Honestidad</label></p>
					<p><input type="radio" name="eval3quest4" value="c"><label>Conocer mis errores y consegui tiempo para perfeccionarme</label></p>
				</li>
				<li class="pregunta-evaluacion">
					<h3>¿Cuál es uno de los objetivos del sistema de organización de ventas?</h3>
					<p><input type="radio" name="eval3quest5" value="a"><label>Que el distribuidor obtenga el máximo rendimiento</label></p>
					<p><input type="radio" name="eval3quest5" value="b"><label>Mejorar el speech al hablar</label></p>
					<p><input type="radio" name="eval3quest5" value="c"><label>Conocer más a fondo a nuestros clientes</label></p>
				</li>
				<li class="pregunta-evaluacion">
					<h3>¿Cada inicio de mes deben de planearse por escrito?</h3>
					<p><input type="radio" name="eval3quest6" value="a"><label>Capacidad de ventas</label></p>
					<p><input type="radio" name="eval3quest6" value="b"><label>Objetivos diarios, semanales, mensuales</label></p>
					<p><input type="radio" name="eval3quest6" value="c"><label>Porcentaje de utilidades</label></p>
				</li>
				<li class="pregunta-evaluacion">
					<h3>¿Cuáles son los dos puntos claves que debemos anializar unos minutos?</h3>
					<p><input type="radio" name="eval3quest7" value="a"><label>Si alcancé los objetivos de ayer y cuáles tengo para hoy</label></p>
					<p><input type="radio" name="eval3quest7" value="b"><label>¿Por qué no vendí ayer?</label></p>
					<p><input type="radio" name="eval3quest7" value="c"><label>Si debería cambiar mi forma de vestir</label></p>
				</li>
				<li class="pregunta-evaluacion">
					<h3>¿Qué debe indicarse en la agenda?</h3>
					<p><input type="radio" name="eval3quest8" value="a"><label>Sitio donde se comerá cada día</label></p>
					<p><input type="radio" name="eval3quest8" value="b"><label>Personas a ver</label></p>
					<p><input type="radio" name="eval3quest8" value="c"><label>Tiempo general y horas de dedicación específica</label></p>
				</li>
				<li class="pregunta-evaluacion">
					<h3>¿Cada cuánto se debe dar segimiento a la agenda?</h3>
					<p><input type="radio" name="eval3quest9" value="a"><label>Diario</label></p>
					<p><input type="radio" name="eval3quest9" value="b"><label>Semana</label></p>
					<p><input type="radio" name="eval3quest9" value="c"><label>Mensual</label></p>
					<p><input type="radio" name="eval3quest9" value="d"><label>Anual</label></p>
				</li>
			</ol>
			<section class="block fichas-container">
				<input type="hidden" name="<?php echo $post->post_name; ?>-nonce" value="<?php echo wp_create_nonce('submit-evaluacion3-nonce') ?>">
				<input class="btn-enviar-eval" type="submit" name="submit-evaluacion3" value="Envía tus respuestas y descubre tu resultado.">
			</section>
		</form>
		<?php endwhile; endif; ?>
	<?php } else { ?>
		<?php echo do_shortcode('[custom-login-form]'); ?>
	<?php } ?>
<?php get_footer(); ?>