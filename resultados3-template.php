<?php
	/*
	Template name: Resultados de evaluación 3
	*/
	get_header();
?>
<?php if(is_user_logged_in()) { ?>
		<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
		<section class="block fichas-container">
			<h1><?php the_title(); ?></h1>
			<?php the_content(); ?>
		</section>			
			<ol class="listado-evaluacion block">
				<li class="pregunta-evaluacion">
					<?php
						$eval3quest1 = get_user_meta(get_current_user_id(),'_eval3quest1',true);
						if($eval3quest1 == 'b')
						{
							?>
								<h3 class="correcta">¿Qué es el M.O.T.E.?</h3>
							<?php
						}
						else 
						{
							?>
								<h3 class="incorrecta">¿Qué es el M.O.T.E.?</h3>
							<?php
						}
					?>					
				</li>
				<li class="pregunta-evaluacion">					
					<?php
						$eval3quest2 = get_user_meta(get_current_user_id(),'_eval3quest2',true);
						if($eval3quest2  == 'a')
						{
							echo '
								<h3 class="correcta">¿Qué es algo que te permite saber una buena organización?</h3>
							';
						}
						else
						{
							echo '
								<h3 class="incorrecta">¿Qué es algo que te permite saber una buena organización?</h3>
							';
						}
					?>					
				</li>
				<li class="pregunta-evaluacion">					
					<?php						
						$eval3quest3 = get_user_meta(get_current_user_id(),'_eval3quest3',true);
						if($eval3quest3 == 'b')
						{
							echo '
								<h3 class="correcta">¿Cuál es la reacción natural que tenemos ante algo que no conocemos?</h3>
							';
						}
						else
						{
							echo '
								<h3 class="incorrecta">¿Cuál es la reacción natural que tenemos ante algo que no conocemos?</h3>
							';
						}
					?>
				</li>
				<li class="pregunta-evaluacion">
					<?php						
						$eval3quest4 = get_user_meta(get_current_user_id(),'_eval3quest4',true);
						if($eval3quest4 == 'c')
						{
							echo '
								<h3 class="correcta">Para aprender a ser organizado debes de aprender 2 cosas importantes, ¿Cuales son?</h3>
							';
						}
						else
						{
							echo '
								<h3 class="incorrecta">Para aprender a ser organizado debes de aprender 2 cosas importantes, ¿Cuales son?</h3>
							';
						}
					?>
				</li>
				<li class="pregunta-evaluacion">
					<?php
						$eval3quest5 = get_user_meta(get_current_user_id(),'_eval3quest5',true);
						if($eval3quest5 == 'a')
						{
							echo '
								<h3 class="correcta">¿Cuál es uno de los objetivos del sistema de organización de ventas?</h3>
							';
						}
						else
						{
							echo '
								<h3 class="incorrecta">¿Cuál es uno de los objetivos del sistema de organización de ventas?</h3>
							';
						}
					?>
				</li>
				<li class="pregunta-evaluacion">
					<?php
						$eval3quest6 = get_user_meta(get_current_user_id(),'_eval3quest6',true);
						if($eval3quest6 == 'b')
						{
							echo '
								<h3 class="correcta">¿Cada inicio de mes deben de planearse por escrito?</h3>
							';
						}
						else {
							echo '
								<h3 class="incorrecta">¿Cada inicio de mes deben de planearse por escrito?</h3>
							';
						}
					?>
				</li>
				<li class="pregunta-evaluacion">
					<?php
						$eval3quest7 = get_user_meta(get_current_user_id(),'_eval3quest7',true);
						if($eval3quest7 == 'a')
						{
							echo '
								<h3 class="correcta">¿Cuáles son los dos puntos claves que debemos anializar unos minutos?</h3>
							';
						}
						else {
							echo '
								<h3 class="incorrecta">¿Cuáles son los dos puntos claves que debemos anializar unos minutos?</h3>
							';
						}
					?>
				</li>
				<li class="pregunta-evaluacion">
					<?php
						$eval3quest8 = get_user_meta(get_current_user_id(),'_eval3quest8',true);
						if($eval3quest8 == 'c')
						{
							echo '
								<h3 class="correcta">¿Qué debe indicarse en la agenda?</h3>
							';
						}
						else {
							echo '
								<h3 class="incorrecta">¿Qué debe indicarse en la agenda?</h3>
							';
						}
					?>
				</li>
				<li class="pregunta-evaluacion">
					<?php
						$eval3quest9 = get_user_meta(get_current_user_id(),'_eval3quest9',true);
						if($eval3quest9 == 'a')
						{
							echo '
								<h3 class="correcta">¿Cada cuánto se debe dar segimiento a la agenda?</h3>
							';
						}
						else {
							echo '
								<h3 class="incorrecta">¿Cada cuánto se debe dar segimiento a la agenda?</h3>
							';
						}
					?>
				</li>
			</ol>
			<section class="block fichas-container">
				<?php
					$resultadoeval3 = get_user_meta(get_current_user_id(),'_resultadoeval3',true);
					$conteo = count($resultadoeval3);					
					if($conteo != '')
					{
						if($conteo > 7)
						{
							echo '
								<p class="correcta"><strong>Resultado de tu evaluación: '.$conteo.' / 9</strong></p>
								<p class="correcta">Tu resultado es aprobatorio</p>
							';
						}
						else
						{
							echo '
								<p class="incorrecta"><strong>Resultado de tu evaluación: '.$conteo.' / 9</strong></p>
								<p class="incorrecta">Tu resultado no es aprobatorio</p>
								<p>Podrás intentar responder correctamente a esta evaluación en 48 horas.</p>
							';
						}
					}				
				?>
				<a class="responde-ahora" href="'.home_url().'/distribuidor-home/evaluaciones/">Regresa a las evaluaciones</a>
			</section>
		<?php endwhile; endif; ?>
	<?php } else { ?>
		<?php echo do_shortcode('[custom-login-form]'); ?>
	<?php } ?>
<?php get_footer(); ?>